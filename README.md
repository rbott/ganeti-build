# Ganeti Nightly Builds Repository

This repository triggers scheduled nightly builds of Ganeti including an (almost) full QA run against a 'real' cluster. This is what currently happens:

* Debian packages are built from the master branch of the upstream Ganeti repository
* These packages are uploaded to an APT repository hosted on [Debian Salsa Gitlab Pages](https://rbott-guest.pages.debian.net/ganeti-build/)
* A three-node Ganeti KVM/DRBD cluster gets bootstrapped using the freshly built packages on dedicated test hardware to run the entire Ganeti QA suite

# Custom Build/Test runs

* by default, the pipeline builds on Debian/Buster from the upstream Ganeti repository's master branch - but that can be customized with variables:
```
#!/bin/bash

# gitlab build trigger token. ask bott@sipgate.de
TOKEN=ENTER_SECRET_TOKEN_HERE

# github organisation/repository to build from
REPOSITORY=ganeti/ganeti
# github branch to build from
BRANCH=master

OS_FLAVOR=debian
OS_RELEASE=buster

curl -X POST \
     -F token=${TOKEN} \
     -F "ref=master" \
     -F "variables[source_repository]=${REPOSITORY}" \
     -F "variables[source_branch]=${BRANCH}" \
     -F "variables[os_flavor]=${OS_FLAVOR}" \
     -F "variables[os_release]=${OS_RELEASE}" \
     https://salsa.debian.org/api/v4/projects/39944/trigger/pipeline
```
Please note, that as of now only Debian/Buster is supported. 

## Trigger script for the pipeline

There is a script available to trigger the pipeline - it will wait for the pipeline to finish and set a return code according to the pipelines status. This way it can be used in automations. The original intentation is/was to use it from Github Actions until the testing workflow can be implemented natively using GH Actions.

Usage is as follows to test the current github.com/ganeti/ganeti master branch:
```
./trigger-pipeline-and-wait-for-it.py --token $TOKEN --project-id 39944 --os-flavor ubuntu --os-release focal
```

You can also specify refs to a Github pull request (Github Actions export that information using the GITHUB_REF environment variable)
```
./trigger-pipeline-and-wait-for-it.py --token $TOKEN --project-id 39944 --os-flavor ubuntu --os-release focal --source-branch refs/pull/1445/merge
```

## Gitlab Runners used for this pipeline

All nodes receive their entire setup from this (private) [Ansible repository](https://github.com/sipgate/ansible-ganeti-community-servers). Currently there are two nodes in use as Gitlab runners (both run/sponsored by sipgate):
* one VM which offers a docker based runner setup for use as a build machine
* one physical server which is used to run the virtualised Ganeti cluster (nested KVM) and the Ganeti QA suite

## The QA suite test environment

All things related to the testing/QA resides in this [repository](https://github.com/sipgate/ganeti-cluster-testing). It contains...
* scripts to set up test VMs (using libvirt, debootstrap etc.)
* Ansible roles/playbooks to setup Ganeti, LVM, DRBD, Networking etc. inside the test VMs
* Ganeti QA configuration files for different test setups (currently only KVM/DRBD/bridged-networking)

In theory, more ansible playbooks/ganeti QA configuration could be created to also test other disk types or networking setups.

## Limitations

* as of now, the [cluster testing repository](https://github.com/sipgate/ganeti-cluster-testing) only contains one usecase (KVM/DRBD/bridged-networking)
* an entire testrun takes around 90 minutes (+ 10 minutes of VM setup)
* currently it uses _noop_ as instance OS type which disables some tests, but using _debootstrap_ would make the entire process even slower
* testing a Xen environment needs separate hardware (if possible at all)
* it is rather complicted to build Gitlab CI/CD matrix builds (e.g. for Debian stable/testing, Ubuntu LTS/stable etc.)
* the current aptly setup only publishes the latest build (and is not suited for matrix-style builds at all)

## TODOs

* provide an aptly repository which always keeps the last N builds and also supports packages for different Debian/Ubuntu versions
* build/test for multiple distributions/releases (partially supported with build variables now)
* move this repository over to Github Actions (where the rest of Ganeti lives). Showstopper: Github Actions do not yet support custom runner labels so you can not distinguish a build runner from the QA/testing runner in your pipeline
