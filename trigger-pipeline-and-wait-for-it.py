#!/usr/bin/python3

import requests
import argparse
from sys import exit
from time import sleep


def parse_arguments():
    parser = argparse.ArgumentParser(description="Trigger and follow ganeti build pipeline on Debian Salsa Gitlab")
    parser.add_argument("--project-id", action="store",
                        default="39944", dest="project_id",
                        help="Gitlab project ID")
    parser.add_argument("--token", required=True, action="store",
                        dest="token", help="Provide Gitlab trigger token")
    parser.add_argument("--os-flavor", action="store",
                        dest="os_flavor", choices=["debian", "ubuntu"],
                        default="debian")
    parser.add_argument("--os-release", action="store",
                        dest="os_release", default="buster",
                        help="the debian/ubuntu release to install")
    parser.add_argument("--source-repository", action="store",
                        dest="source_repository", default="ganeti/ganeti",
                        help="which repository to build from")
    parser.add_argument("--source-branch", action="store",
                        dest="source_branch", default="master",
                        help="which branch to build from")
    parser.add_argument("--qa-recipe", action="store",
                        dest="qa_recipe",
                        default="kvm-drbd_file_sharedfile-bridged",
                        help="set the name of the QA recipe")

    return parser.parse_args()


def trigger_job(project_id, token, os_flavor, os_release,
                source_repository, source_branch, qa_recipe):
    parameters = {
        "token": token,
        "ref": "master",
        "variables[build_os_flavor]": os_flavor,
        "variables[build_os_release]": os_release,
        "variables[source_repository]": source_repository,
        "variables[source_branch]": source_branch,
        "variables[qa_recipe]": qa_recipe
    }
    r = requests.post("https://salsa.debian.org/api/v4/projects/{}/trigger/pipeline".format(project_id), data=parameters)

    if r.status_code >= 200 and r.status_code <= 299:
        return r.json()
    else:
        print("Gitlab API returned HTTP {}".format(r.status_code))
        print("URL was: {}".format(r.url))
        print("Output was: {}".format(r.text))
        exit(1)


def wait_for_pipeline_to_finish(project_id, pipeline_id):
    timer = 0
    status = "pending"
    waiting_status = ["pending", "running"]
    while status in waiting_status:
        sleep(5)
        timer = timer + 5
        if timer > 7200:
            print("Timed out after waiting for two hours, bye!")
            exit(1)
        r = requests.get("https://salsa.debian.org/api/v4/projects/{}/pipelines/{}".format(project_id, pipeline_id))
        if r.status_code >= 200 and r.status_code <= 299:
            pipeline_data = r.json()
            if status != pipeline_data["status"]:
                print("Pipeline status changed from {} to {}".format(
                    status, pipeline_data["status"]))
            status = pipeline_data["status"]
        else:
            status = "failed"
            break

    return status


def main():
    args = parse_arguments()

    pipeline_data = trigger_job(args.project_id, args.token, args.os_flavor,
                                args.os_release, args.source_repository,
                                args.source_branch, args.qa_recipe)
    print("Started Pipeline with ID {} - See full output at {}".format(
        pipeline_data["id"], pipeline_data["web_url"]))

    bad_status = ["failed", "canceled", "skipped"]
    status = wait_for_pipeline_to_finish(args.project_id, pipeline_data["id"])
    if status in bad_status:
        exit(1)


if __name__ == "__main__":
    main()
